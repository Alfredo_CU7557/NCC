# NCC.
- Correlación Cruzada Normalizada (NCC) es un método que se utiliza para medir la similitud entre dos imágenes.

## Funcionamiento.

![Funcionamiento](Img/dec.png)

## Resultado.

![Resultado](Img/1.png)
